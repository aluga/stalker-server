def convertYUVtoRGB(yuv, width=640, height=480):
    out = [0]*(width * height)
    sz = width * height

    Y=0
    Cr = 0
    Cb = 0
    for j in range(height):
        pixPtr = j * width
        jDiv2 = j >> 1
        for i in range(width):
            Y = yuv[pixPtr]
            if (Y < 0):
                Y += 255
            if ((i & 0x1) != 1):
                cOff = sz + jDiv2 * width + (i >> 1) * 2
                Cb = yuv[cOff]
                if (Cb < 0):
                    Cb += 127
                else:
                    Cb -= 128
                Cr = yuv[cOff + 1]
                if (Cr < 0):
                    Cr += 127
                else:
                    Cr -= 128
            R = Y + Cr + (Cr >> 2) + (Cr >> 3) + (Cr >> 5)
            if (R < 0):
                R = 0
            elif (R > 255):
                R = 255
            G = Y - (Cb >> 2) + (Cb >> 4) + (Cb >> 5) - (Cr >> 1) \
                    + (Cr >> 3) + (Cr >> 4) + (Cr >> 5);
            if (G < 0):
                G = 0
            elif (G > 255):
                G = 255
            B = Y + Cb + (Cb >> 1) + (Cb >> 2) + (Cb >> 6)
            if (B < 0):
                B = 0
            elif (B > 255):
                B = 255
            out[pixPtr] = [R,G,B]
            pixPtr +=1
    return out
