from __future__ import print_function

from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

from kafka import SimpleProducer, KafkaClient
from kafka import KafkaProducer

import cv2
import numpy as np

import widetra

producer = KafkaProducer(bootstrap_servers='kafka:9092')

# Tracker
address_size = 8
min_0 = 1
min_1 = 0
search_size = 5
max_discriminators = 1
max_training = 0
new_discriminator = 0.2
new_training = 0.9
min_score = 0.1
initial_scale = 0.2

# Detector
# addressSize_d = 8
# min_0_d = 1
# min_1_d = 0
# step_d = 4
# binaryFunc_d = 0
# frame_scale_d = 0.6

frame = 0
tracker = None

def sendBack(img):
    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 50]
    result, encimg = cv2.imencode('.jpg', img, encode_param)
    producer.send('tracker', value=bytes(encimg))
    producer.flush()

def updateTracker(img, sel=[]):
    global tracker
    if tracker != None:
        sel = tracker.find(img)
    elif len(sel) != 0:
        tracker = widetra.Tracker(img, sel,  max_discriminators, address_size, search_size, new_discriminator, new_training, max_training, min_score, min_0, min_1, initial_scale)

    if len(sel) != 0:
        cv2.rectangle(img, (sel[0], sel[1]), (sel[2], sel[3]), (0, 0, 255));

    return img

def tracking(message):
    global frame
    records = message.collect()
    for record in records:
        frame += 1
        print("Frame {}".format(frame))

        img = []
        selection = []

        if record[0] == "data":
            img = cv2.imdecode(np.frombuffer(record[1], np.uint8), -1)

            img = updateTracker(img)
            sendBack(img)
        else:
            img = cv2.imdecode(np.frombuffer(record[1][16:], np.uint8), -1)

            selection = [int.from_bytes(record[1][0:4], byteorder='big', signed=True),
                        int.from_bytes(record[1][4:8], byteorder='big', signed=True),
                        int.from_bytes(record[1][8:12], byteorder='big', signed=True),
                        int.from_bytes(record[1][12:16], byteorder='big', signed=True)]
            print("selection: ",selection)

            img = updateTracker(img, selection)
            sendBack(img)

if __name__ == "__main__":
    spark = SparkSession.builder.appName("tracker").getOrCreate()
    sc = spark.sparkContext
    ssc = StreamingContext(sc, 1)

    brokers, topic = ("kafka:9092","video")
    kafkaStream = KafkaUtils.createDirectStream(ssc, [topic], {"metadata.broker.list": brokers}, valueDecoder= lambda x : x)
    data = kafkaStream.map(lambda x: [x[0],x[1]])

    data.foreachRDD(tracking)

    ssc.start()
    ssc.awaitTermination()
