const kafka = require("kafka-node");
const express = require('express');
const net = require('net');
// const client = new kafka.Client("kafka:2181");
// const client = new kafka.KafkaClient({kafkaHost: 'kafka:9092'});


function sleep(ms){
    return new Promise(resolve => setTimeout(resolve, ms));
}

const kafkaHost = 'kafka:9092';
const HOST = '0.0.0.0';
const inputTopic = "video";
const outputTopic = "tracker";// change to 'tracker'

var client = new kafka.KafkaClient({kafkaHost: kafkaHost});
const producer = new kafka.Producer(client);
producer.on('ready',function(){
  producer.createTopics([inputTopic, outputTopic], true, function (err, data) {
    if(data){
      console.log("created topics: ",data);
    }
  });
});
producer.on("error",function(err){
  console.log("Producer error!!!");
});

// http server config
var app = express();
app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(3000, function () {
  console.log('HTTP Server listening on port 3000');
});

// create socker connection to real time comunication
// input stream server
const PORT3 = 3003;
const FRAME_SIZE = 307200;
const INT32 = 4;
net.createServer(function(sock) {
  // Receives a connection - a socket object is associated to the connection automatically
  console.log('INPUT STREAM CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);
  sock.configImage = null;
  sock.configBuff = Buffer.alloc(0);
  sock.buff = Buffer.alloc(0);
  sock.frameSize = FRAME_SIZE;
  var client = new kafka.KafkaClient({kafkaHost: kafkaHost});
  sock.producer = new kafka.Producer(client);
  sock.producer.on("error",function(err){
    console.log("Producer error!!!");
    sock.destroy();
  });
  // Add a 'data' - "event handler" in this socket instance
  sock.on('data', function(data) {
	  // data was received in the socket
	  // Writes the received message back to the socket (echo)
    if(sock.configImage == null){
      if(sock.configBuff.length == 0) {
        sock.write('{"state":"ok"}');
      }
      var stop = data.indexOf('}');
      if(stop == -1){
        sock.configBuff = Buffer.concat([sock.configBuff, data]);
      }
      else{
        stop++;
        var size = stop + sock.configBuff.length;
        sock.configBuff = Buffer.concat([sock.configBuff, data], size);
        sock.buff = data.slice(stop);
        sock.configImage = JSON.parse(sock.configBuff.toString());
        sock.frameSize = sock.configImage.length;
        console.log("config: ",sock.configImage);
      }
    }
    else{
      if(sock.buff.length < INT32 || sock.buff.length <= sock.buff.readUInt32BE(0)+INT32){
        sock.buff = Buffer.concat([sock.buff,data]);
      }
      if(sock.buff.length > INT32 && sock.buff.length >= sock.buff.readUInt32BE(0)+INT32){
        var cut = sock.buff.readUInt32BE(0)+INT32;
        var frameData = sock.buff.slice(INT32,cut);
        if(sock.configImage.type == "detect"){
          sock.producer.send([{topic: inputTopic, messages: new kafka.KeyedMessage('detect',frameData)}], function(err,data){
            if(data)
              console.log("detect sended!");
            else{
              console.log("detect not sended!");
            }
          });
          sock.destroy();
        }
        else{
          if(frameData[0]!=255||frameData[1]!=216||frameData[frameData.length-2]!=255||frameData[frameData.length-1]!=217){
            sock.destroy();
            console.log("Error in parser so the connection was closed!");
            return;
          }
          sock.producer.send([{topic: inputTopic, messages: new kafka.KeyedMessage('data',frameData)}], function(err,data){
            if(data)
              console.log("frame sended!");
            else{
              console.log("frame not sended!");
            }
          });

          console.log("frame data: ", sock.buff.readUInt32BE(0), frameData[0], frameData[1], frameData[frameData.length-2], frameData[frameData.length-1]);
          sock.buff = sock.buff.slice(cut);
        }
      }
    }
  });

  // Add a 'close' - "event handler" in this socket instance
  sock.on('close', function(data) {
	  // closed connection
	  console.log('INPUT STREAM CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
  });
}).listen(PORT3, HOST);


console.log('TCP Server input stream listening on ' + PORT3);

// output stream server
const PORT4 = 3006;
net.createServer(function(sock) {
  console.log('OUTPUT STREAM CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);
  var client = new kafka.KafkaClient({kafkaHost: kafkaHost});
  sock.consumer = new kafka.Consumer(client,[{topic:outputTopic}],{
    fromOffset:false,encoding:"buffer",
    groupId: 'tracker-group',
    autoCommit: true,
    autoCommitIntervalMs: 100});
  sock.consumer.offset = null;
  sock.consumer.on("error",function(err){
    console.log("Consumer error!!!");
  });

  sock.on('data', function(data){
    sock.trackerId = data;
    console.log("getting tracker!");
    sock.consumer.on("message", function(message){
      //if(sock.consumer.counter == null) sock.consumer.counter = message.offset;
      //if(sock.consumer.counter > message.offset || message.value.length==0) return;
      var data = message.value;
      if(data[0]!=255||data[1]!=216||data[data.length-2]!=255||data[data.length-1]!=217) return;
      var len = Buffer.alloc(4);
      len.writeInt32BE(data.length);
      if(sock && !sock.destroyed){
        sock.write(Buffer.concat([len,data]));
      }
      console.log("consume: ",data.length, data[0],data[1],data[data.length-2],data[data.length-1]);
      sock.consumer.offset=message.offset;
    });

  });

  sock.on('error', function(err){
    console.log("error:",err);
  });

  sock.on('close', function(data) {
    sock.consumer.close(function(){
      console.log("consumer connection closed!");
    });
	  console.log('OUTPUT STREAM CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
  });
}).listen(PORT4, HOST);

console.log('TCP Server output stream listening on ' + PORT4);
