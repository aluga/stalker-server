# Stalker Server

# To start it
  docker-compose up -d kafka
  docker-compose up -d node
  docker-compose up -d pyspark

To this works you need install [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) and [docker-compose](https://docs.docker.com/compose/install/#install-compose).
In the first run the system will create the images of each server, so this will take a while and you need internet connection.
